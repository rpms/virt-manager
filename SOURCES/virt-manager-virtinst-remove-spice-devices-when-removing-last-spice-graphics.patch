From e3734f6e9120ed62238bcffb6bd0679b64b42c70 Mon Sep 17 00:00:00 2001
Message-ID: <e3734f6e9120ed62238bcffb6bd0679b64b42c70.1737975657.git.phrdina@redhat.com>
From: Pavel Hrdina <phrdina@redhat.com>
Date: Mon, 25 Mar 2024 14:39:20 +0100
Subject: [PATCH] virtinst: remove spice devices when removing last spice
 graphics

When Spice graphics is used QEMU creates a Spice server and communicates
with Spice client using multiple channels. These channels are used by
the spice devices as well. Without the Spice graphics defined there is
no use for the other devices. In addition libvirt will report error for
such configuration.

Signed-off-by: Pavel Hrdina <phrdina@redhat.com>
(cherry picked from commit aaf85519142d672e7486020d9847358f36df4f70)

https://issues.redhat.com/browse/RHEL-17435

Signed-off-by: Pavel Hrdina <phrdina@redhat.com>
---
 .../virt-xml-remove-spice-graphics.xml        | 18 ++++++++++----
 virtinst/guest.py                             | 24 +++++++++++++++++++
 2 files changed, 38 insertions(+), 4 deletions(-)

diff --git a/tests/data/cli/compare/virt-xml-remove-spice-graphics.xml b/tests/data/cli/compare/virt-xml-remove-spice-graphics.xml
index ed9f2a584..3d27d4121 100644
--- a/tests/data/cli/compare/virt-xml-remove-spice-graphics.xml
+++ b/tests/data/cli/compare/virt-xml-remove-spice-graphics.xml
@@ -1,14 +1,24 @@
-     <channel type="spicevmc">
-       <target type="virtio" name="com.redhat.spice.0"/>
-     </channel>
+     <emulator>/usr/bin/qemu-system-x86_64</emulator>
+     <controller type="usb" index="0" model="qemu-xhci" ports="15"/>
+     <controller type="virtio-serial" index="0"/>
+-    <channel type="spicevmc">
+-      <target type="virtio" name="com.redhat.spice.0"/>
+-    </channel>
 -    <graphics type="spice" autoport="yes">
 -      <listen type="address"/>
 -      <image compression="off"/>
 -      <gl enable="no"/>
 -    </graphics>
-     <audio id="1" type="spice"/>
+-    <audio id="1" type="spice"/>
      <video>
        <model type="qxl" ram="65536" vram="65536" vgamem="16384" heads="1" primary="yes"/>
+     </video>
+-    <redirdev bus="usb" type="spicevmc">
+-    </redirdev>
+-    <redirdev bus="usb" type="spicevmc">
+-    </redirdev>
+   </devices>
+ </domain>
 
 Domain 'test-spice' defined successfully.
 Changes will take effect after the domain is fully powered off.
diff --git a/virtinst/guest.py b/virtinst/guest.py
index fc7b0080a..7aa62be49 100644
--- a/virtinst/guest.py
+++ b/virtinst/guest.py
@@ -483,6 +483,7 @@ class Guest(XMLBuilder):
     def remove_device(self, dev):
         self.devices.remove_child(dev)
         self._remove_duplicate_console(dev)
+        self._remove_spice_devices(dev)
 
     devices = XMLChildProperty(_DomainDevices, is_single=True)
 
@@ -1044,3 +1045,26 @@ class Guest(XMLBuilder):
         if condup:
             log.debug("Found duplicate console device:\n%s", condup.get_xml())
             self.devices.remove_child(condup)
+
+    def _remove_spice_audio(self):
+        for audio in self.devices.audio:
+            if audio.type == "spice":
+                self.devices.remove_child(audio)
+
+    def _remove_spice_channels(self):
+        for channel in self.devices.channel:
+            if channel.type == DeviceChannel.TYPE_SPICEVMC:
+                self.devices.remove_child(channel)
+
+    def _remove_spice_usbredir(self):
+        for redirdev in self.devices.redirdev:
+            if redirdev.type == "spicevmc":
+                self.devices.remove_child(redirdev)
+
+    def _remove_spice_devices(self, rmdev):
+        if rmdev.DEVICE_TYPE != "graphics" or self.has_spice():
+            return
+
+        self._remove_spice_audio()
+        self._remove_spice_channels()
+        self._remove_spice_usbredir()
-- 
2.48.1
